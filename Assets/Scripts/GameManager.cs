﻿
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour{

    private PressBtn _pressBtn;
    public Image imgMain;
    public State state;
    public Text questionText;
    public Text choiceText;

    public GameObject btn1;
    public GameObject btn2;
    public GameObject btn3;
    public GameObject btn4;

    private void Start() {
        StateInit();
    }

    public void BtnClick1() {
        state = state.state1;
        StoryBody();
    }
    public void BtnClick2() {
        state = state.state2;
        StoryBody();
    }
    
    public void BtnClick3() {
        state            = state.state3;
        StoryBody();
    }
    
    public void BtnClick4() {
        state = state.state4;
        StoryBody();
    }

    private void StoryBody() {
        choiceText.text   = state.textVibor;
        questionText.text = state.storyText;
        imgMain.sprite      = state.imageMain;
        btn1.GetComponentInChildren<Text>().text = state.btn1;
        btn2.GetComponentInChildren<Text>().text = state.btn2;
        btn3.GetComponentInChildren<Text>().text = state.btn3;
        btn4.GetComponentInChildren<Text>().text = state.btn4;
    
        if(state.win == 1) {
            print("Ура мы победили");
        }

        if(state.win == 2) {
            print("печаль мы проиграли");
        }

        if(state.ofBtn == 2) {
            print("выключить кнопки");
            btn3.SetActive(false);
            btn4.SetActive(false);
        }
    }
    
    private void StateInit() {
        questionText.text = state.storyText;
        choiceText.text   = state.textVibor;
        imgMain.sprite      = state.imageMain;

        btn1.GetComponentInChildren<Text>().text = state.btn1;
        btn2.GetComponentInChildren<Text>().text = state.btn2;
        btn3.GetComponentInChildren<Text>().text = state.btn3;
        btn4.GetComponentInChildren<Text>().text = state.btn4;
        
        PressBtn.Instance.GetBtn1 += BtnClick1;
        PressBtn.Instance.GetBtn2 += BtnClick2;
        PressBtn.Instance.GetBtn3 += BtnClick3;
        PressBtn.Instance.GetBtn4 += BtnClick4;
    }
}
